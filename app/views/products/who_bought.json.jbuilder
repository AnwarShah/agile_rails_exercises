json.array! @product.orders do |order|
  json.array! order.line_items do |item|
    json.extract! item, :product
    json.extract! item, :quantity, :total_price
  end
end
