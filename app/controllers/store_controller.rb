class StoreController < ApplicationController
  include CurrentCart
  before_action :set_cart
  after_action :increment_index_hit_count, only: [:index]

  def index
    @products = Product.order(:title)
  end

  private 

  	def increment_index_hit_count
  		session[:counter] = session[:counter].nil? ? 1 : 
  			session[:counter] + 1
  	end
end
